@@include('webp_img.js');
@@include('swiper.js');
@@include('parallax.js');
@@include('jquery-modal.js');
@@include('jquery.mask.js');
@@include('libs/jquery.lazy.min.js');
@@include('libs/jquery.lazy.plugins.min.js');

document.addEventListener("DOMContentLoaded", function (event) {
  const swiper = new Swiper('.projects__slider', {
    slidesPerView: 3,
    // watchSlidesVisibility: true,
    spaceBetween: 16,
    loop: false,

    // Navigation arrows
    navigation: {
      nextEl: '.projects__next',
      prevEl: '.projects__prev',
    },

    breakpoints: {
      // when window width is >= 0px
      0: {
        slidesPerView: 2,
        spaceBetween: 16,
        loop: true
      },
      // when window width is >= 480px
      1200: {
        slidesPerView: 3,
        loop: false
      }
    }


  });

  const swiper2 = new Swiper('.reviews__slider', {
    slidesPerView: 2,
    // watchSlidesVisibility: true,
    spaceBetween: 16,
    loop: false,



    // Navigation arrows
    navigation: {
      nextEl: '.reviews__next',
      prevEl: '.reviews__prev',
    }



  });

  const swiper3 = new Swiper('.work-with__cards_m', {
    slidesPerView: 2,
    spaceBetween: 16,
    autoplay: {
      delay: 5000,
    },
    loop: true

  });

  // === calculator ===

  const ad_budget = document.querySelector('#ad_budget'),
        ad_count = document.querySelector('#ad_count'),
        price_click = document.querySelector('#price_click'), 
        steps = document.querySelector('#steps'),
        ctr = document.querySelector('#ctr'),
        lead = document.querySelector('#lead'),
        conversion = document.querySelector('#conversion'),
        apps = document.querySelector('#apps'),
        sales_conversion = document.querySelector('#sales_conversion'),
        clients = document.querySelector('#clients'),
        avg_check = document.querySelector('#avg_check'),
        client_cost = document.querySelector('#client_cost'),
        rent = document.querySelector('#rent'),
        net_profit = document.querySelector('#net_profit'),
        result = document.querySelector('#result'),
        allInputs = document.querySelectorAll('.calculator__input');

  let activeInputs = [ad_budget, ad_count, sales_conversion, avg_check, rent];

  
  activeInputs.forEach((element) => {

    let addSpace = () => {
      $('.calculator__input').each(function() {
        let curr = this.value;
        this.value = String(curr).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
      })
    }
    addSpace();
    element.oninput = function() {
      allInputs.forEach((e) => {
        e.value = e.value.replace(/\s+/g, '').trim()
      })

      if (element.name != "ad_count") {
        steps.value = Math.round(ad_budget.value / price_click.value)

        ad_count.value = Math.round(steps.value / (ctr.value / 100))
      } else {
        steps.value = Math.round(ad_count.value * ctr.value / 100)

        ad_budget.value = Math.round(steps.value * price_click.value)
      }

      apps.value = Math.round(conversion.value / 100 * steps.value)

      lead.value = Math.round(ad_budget.value / apps.value)

      clients.value = Math.round(sales_conversion.value / 100 * apps.value)

      client_cost.value = Math.round(ad_budget.value / clients.value)

      net_profit.value = Math.round(clients.value * avg_check.value * rent.value / 100 - ad_budget.value)

      result.innerText = `ROI = ${Math.round(net_profit.value / ad_budget.value * 100)} %`;



      addSpace();

    }

    element.addEventListener('keydown', function(event) {
      // Разрешаем: backspace, delete, tab и escape
      if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
        // Разрешаем: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Разрешаем: home, end, влево, вправо
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        
        // Ничего не делаем
        return;
      } else {
        // Запрещаем все, кроме цифр на основной клавиатуре, а так же Num-клавиатуре
        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
          event.preventDefault();
        }
      }
    });
  })

  // === / calculator ===

  // === count projects left ===

  let works_in_progress = document.querySelector('#now'),
      works_max = document.querySelector('#all'),
      works_left = document.querySelector('#left');
  
  let countWorks = () => {
    works_in_progress = Number(works_in_progress.innerText);
    works_max = Number(works_max.innerText);
    return works_left.innerHTML = works_max - works_in_progress;
  }

  countWorks();
  
  // === / count projects left ===

  // === work-with grid ===

  let cards = document.querySelectorAll('.work-with__card');
  cards[cards.length - 1].classList.add('work-with_last')


  if (cards.length % 3 !== 0) {
    let left = 3 - cards.length % 3;
    for (let i = 0; i < left; i++) {
      $('.work-with_last').after('<div class="work-with__card"></div>')
    }
  }

  // === / work-with grid ===
   
  // === burger menu === 

  const header = document.querySelector('.header');
  const burger = document.querySelector('.hamburger');

  burger.addEventListener('click', () => {
    header.classList.toggle('active');
    burger.classList.toggle('active');
  })
  
  // === / burger menu ===
  
  // === parallax settings ===

  const scene = document.querySelector('.scene');
    let parallaxInstance = new Parallax(scene, {
      relativeInput: true
    });

  	
  let scene2 = document.querySelector('.classic__parallax-block');
  let parallaxInstance2 = new Parallax(scene2, {
    relativeInput: true
  });

  let scene3 = document.querySelector('.benefits__parallax-block');
  let parallaxInstance3 = new Parallax(scene3, {
    relativeInput: true
  });

  // === parallax settings ===

  // === reviews accordeon ===

  const all_reviews = document.querySelectorAll('.reviews__review-card');
  const review_button = document.querySelector('.reviews__mobile-button');

  review_button.addEventListener('click', () => {
    all_reviews.forEach(element => {
      element.classList.toggle('reviews__review-card_active')
    })

    if (review_button.innerHTML === 'Читать ещё') {
      review_button.innerHTML = 'Скрыть'
    } else {
      review_button.innerHTML = 'Читать ещё'
    }
  })

  // === / reviews accordeon ===


  // === modal button ===

  let header_btn = document.querySelector('.header__button'),
      hero_btn = document.querySelector('.hero__button'),
      request_btn = document.querySelector('.request__button'),
      calc_btn = document.querySelector('.calculator__button');
      
  
  let modal_btns = [header_btn, hero_btn, request_btn, calc_btn];
  modal_btns.forEach(element => {
    element.addEventListener('click', () => {
      modal_anim();
    });
  })

  let modal_anim = () => {
    $("#modal_window").modal({
      fadeDuration: 300
    });  
  }

  // === / modal button ===

  $('.input__phone').mask('+ 7 000 000-00-00');


  $(function($) {
    $(".lazy").Lazy();
  });


  var $page = $('html, body');
  $('a[href*="#"]').click(function() {
      $page.animate({
          scrollTop: $($.attr(this, 'href')).offset().top
      }, 400);
      return false;
  });
});


